require('dotenv').config()
const express = require('express')
, path = require('path')
, { createServer } = require('http')

const app = express()

app.use(express.static('dist'))

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist', 'index.html'))
})

const httpServer = createServer(app)
server.installSubscriptionHandlers(httpServer)

httpServer.listen({ port: 2000 }, () => {
  console.log(`🤖 Server ready at http://localhost:2000`)
})
