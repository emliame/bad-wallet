import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Messages from '@/pages/Messages'
import Settings from '@/pages/Settings'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/messages',
      name: 'Messages',
      component: Messages
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
  ]
})
