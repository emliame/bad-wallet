import Vue from 'vue'

const storageKey = 'bw-acct'

export default {

  // Generic Update
  update({ commit }, { key, value }) {
    commit('UPDATE', { key, value })
  },

  // new Wallet
  createWallet({ commit }) {
    const eth = Vue.prototype.$ethers
    const wallet = eth.Wallet.createRandom()
    commit('UPDATE', { key: 'account', value: wallet.signingKey })
    commit('UPDATE', { key: 'Wallet', value: new eth.Wallet(wallet.signingKey.privateKey) })
    localStorage.setItem(storageKey, JSON.stringify(wallet.signingKey))
  },

  // There are 3 ways to load a wallet:
  // 1. privateKey
  // 2. jsonWallet file + password
  // 3. mnemonic phrase
  //
  // attaching provider allows connection to network
  loadWallet({ commit }, {
    privateKey,
    jsonWallet,
    password,
    mnemonic,
    provider,
  }) {
    const eth = Vue.prototype.$ethers
    let wallet

    if (privateKey) {
      wallet = provider
        ? new eth.Wallet(privateKey, provider)
        : new eth.Wallet(privateKey)
    }

    if (jsonWallet && password) {
      eth.Wallet.fromEncryptedJson(jsonWallet, password).then(w => {
          console.log("Address: " + w.address)
          wallet = w
      })
    }

    if (mnemonic) {
      wallet = eth.Wallet.fromMnemonic(mnemonic)
    }

    if (!wallet || !wallet.signingKey) throw "No wallet loaded!"
    commit('UPDATE', { key: 'account', value: wallet.signingKey })
    commit('UPDATE', { key: 'Wallet', value: new eth.Wallet(wallet.signingKey.privateKey) })
    localStorage.setItem(storageKey, JSON.stringify(wallet.signingKey))
  },

  loadWalletFromCache({ commit, state }) {
    if (state.account) return
    const eth = Vue.prototype.$ethers

    try {
      const data = localStorage.getItem(storageKey)
      const account = JSON.parse(data)
      if (!account || !account.address) return
      commit('UPDATE', { key: 'account', value: account })
      commit('UPDATE', { key: 'Wallet', value: new eth.Wallet(account.privateKey) })
    } catch (e) {
      console.log('loadWalletFromCache', e)
    }
  },

  async signData({ state }, data) {
    const signature = await state.Wallet.signMessage(`${data}`)
    console.log('signature', signature)
    return signature
  },

  async verifyDataSigner({ state }, { address, signature, data }) {
    console.log('address, signature, data', address, signature, data)
    const signingAddress = await Vue.prototype.$ethers.utils.verifyMessage(data, signature)
    console.log('address === signingAddress', address, signingAddress, address === signingAddress)
    return address === signingAddress
  }
}
