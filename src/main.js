import { ethers } from 'ethers'
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import store from './store'
import router from './router'
import App from './App'
import ViewUI from 'view-design'
import locale from './locale.js'
import 'view-design/dist/styles/iview.css'
import './theme/index.css'

Vue.config.productionTip = false
Vue.prototype.$ethers = ethers
Vue.prototype.$SEA = SEA // TODO: check how to import, was getting webpack issues
Vue.use(ViewUI, { locale })

// for fun:
window.ethers = ethers

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  store,
  router,
})
