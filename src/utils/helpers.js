export const getElapsedTime = (start, end) => {
  return Math.round((+new Date(end) - +new Date(start)) / 1000 / 60)
}
